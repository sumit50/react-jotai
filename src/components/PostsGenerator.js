import '../style/parent.style.css';
import axios from "axios";
import {useAtom} from "jotai";
import {postsAtom} from "../atoms/atom";
import {Link} from "react-router-dom";
const PostsGenerator = ()=>{
    const [posts,setPosts] = useAtom(postsAtom);
    const fetchPosts = ()=>{
        axios.get("https://jsonplaceholder.typicode.com/posts/")
            .then(res => setPosts(res.data))
            .catch(err=>console.log(err));
    }
    const clearPosts = ()=>{
        setPosts([]);
    }
    return(

        <div>
            <div className = "main-section">
                <div>
                    <button className="main-section-btn" onClick={()=> fetchPosts()}>
                        Generate Posts
                    </button>
                </div>
                <div>
                    <button className="main-section-btn" onClick={()=> clearPosts()}>
                        Clear Posts
                    </button>
                </div>
                <div>
                    <Link to={'/child'}>
                        <button className="main-section-btn" >
                            Navigate to Child
                        </button>
                    </Link>

                </div>
                <div>
                    <Link to={'/'}>
                        <button className="main-section-btn" >
                            Navigate to Parent
                        </button>
                    </Link>

                </div>
            </div>
        </div>
    );
}
export default PostsGenerator;