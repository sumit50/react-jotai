import {useAtom} from "jotai";
import {postsAtom} from "../atoms/atom";
import '../style/post.style.css';

const PostsMap = ()=>{
    const [posts,setPosts] = useAtom(postsAtom);
    console.log(posts);
    return(
        <div>
            <ul className="post-ul">
                {posts.map(post =>
                    <li  key={post.id} className="post-li">
                        <div className="post-container">
                            <div className="post-header">#Post {post.id}</div>
                            <div>{post.title}</div>
                            <div>{post.body}</div>

                        </div>

                    </li>
                )}
            </ul>
        </div>
    );
}
export default PostsMap;