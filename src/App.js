import './App.css';
import React from 'react';
import {Provider} from "jotai";
import Parent from "./containers/Parent";
import {BrowserRouter as Router, Switch,Route} from "react-router-dom";
import Child from "./containers/Child";
function App() {
  return (
        <React.Fragment>
          <Provider>
              <Router>
                  <Switch>
                      <div className="App">
                      <Route path="/" exact component={Parent} />
                      <Route path="/child" exact component={Child} />
                    </div>
                  </Switch>
              </Router>

          </Provider>
        </React.Fragment>

  );
}

export default App;
