import '../style/parent.style.css';
import axios from "axios";
import {useAtom} from "jotai";
import {postsAtom} from "../atoms/atom";
import PostsMap from "../components/PostsMap";
import PostsGenerator from "../components/PostsGenerator";
import Navbar from "./Navbar";

const Parent = ()=>{
    return(
        <div>
            <Navbar/>
            <div className="main-section-title">
                Parent Component
            </div>
            <PostsMap/>
           <PostsGenerator/>
        </div>
    );
}
export default Parent;