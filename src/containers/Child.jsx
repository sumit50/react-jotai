import PostsMap from "../components/PostsMap";
import PostsGenerator from "../components/PostsGenerator";
import Navbar from "./Navbar";

const Child = ()=>{
    return(
        <div>
            <Navbar/>
            <div className="main-section-title">
                Child Component
            </div>
            <PostsMap/>
            <PostsGenerator/>
        </div>
    );
}
export default Child;


